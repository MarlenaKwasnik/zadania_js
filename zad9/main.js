const init = (() => {
    class Account{
        constructor(balance, currency, number) 
           {
               this.balance = balance;
               this.currency = currency;
               this.number = number;
           } 
          
       }
    
    class Person {
        constructor(firstName,lastName,accountsList ) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.accountsList = accountsList;
        }
    
        _calculateBalance() {
           var sum = 0;
           for(var konto of this.accountsList)
           {
             sum = sum + konto.balance;
           }
           return sum; 
        }
    
        
        _sayHello() {
            return "Name: " + this.firstName + ", Surname: " + this.lastName + ", Account: " +  this.accountsList.length + ", Sum: " + this._calculateBalance();
        }
    
        _addAccount(account) {
            return this.accountsList.push(account);
        }
    
        filterPositiveAccounts() {
            return this.accountsList.filter(konto  => konto.balance > 0);   
    
        }
    
        findAccount(accountNumber) {
            return this.accountsList.find(numer => numer.number === accountNumber);
        }
    
        withdraw(accountNumber, amount) {
            return new Promise((resolve, reject) => {
                var personsAccount = this.findAccount(accountNumber);
                    if(personsAccount != undefined && amount < (personsAccount.balance)){ 
                    personsAccount.balance = personsAccount.balance - amount;
                        setTimeout(() => (resolve("Konto:" + accountNumber + "nowa kwota: " + personsAccount.balance + "po withdrawal: " + amount)),3000);
                    }  
                    else {
                    reject("Nie ma takiego konta lub nie masz takiej gotówki"  );
                    }
             })
        }
        
    };
    
    konto = new Account (5000, "$", 1);
    konto1 = new Account (10000, "$", 2);
    konto2 = new Account (3000, "$", 3);
    
    const osoba = new Person("Jan", "Nowak", [konto,konto1,konto2]);
    osoba._addAccount(new Account  (50000, "$", 4));

   // osoba.filterPositiveAccounts();
    window.onload = wypisz_dane();
    function wypisz_dane()
    {
        document.getElementById("dane").innerHTML = "Imię: "+ osoba.firstName + ", Nazwisko: " +  osoba.lastName;
        document.getElementById("ilosc").innerHTML = osoba.accountsList.length;
        document.getElementById("klik").disabled = true;
        for(accountsList of osoba.accountsList){
            var nowa_linia = document.createElement("p");
            var text = document.createTextNode("Konto " + accountsList.number + ": " +  accountsList.balance + " " + accountsList.currency );
            
            nowa_linia.appendChild(text);
            document.getElementById("konta").appendChild(nowa_linia);
        }
    }    

    function odswiez()
    {
        document.getElementById("konta").innerHTML = "";
        for(accountsList of osoba.accountsList){
            var nowa_linia = document.createElement("p");
            var text = document.createTextNode("Konto " + accountsList.number + ": " +  accountsList.balance + " " + accountsList.currency );
            nowa_linia.appendChild(text);
            document.getElementById("konta").appendChild(nowa_linia);
        }  
    }   

    return {
        onSubmitted: function()
        {   
            var numer_konta = parseInt(document.getElementById("numer_konta").value);
            var kwota = document.getElementById("amount").value;
        
            osoba.withdraw(numer_konta,kwota)
            .then(function(powodzenie){
            document.getElementById("komunikat").innerHTML = "Obecny stan konta " + numer_konta + ": " + stan_konta2 + " " + waluta ;

            odswiez();
           
            })  
            .catch(function(porazka){
             document.getElementById("komunikat").innerHTML = porazka;
            });
          
            var stan_konta2 = osoba.findAccount(numer_konta).balance;
            var waluta = osoba.findAccount(numer_konta).currency;
            document.getElementById("komunikat").innerHTML = "Obecny stan konta " + numer_konta + ": " + stan_konta2 + " " + waluta + "<br />"+ "Odświeżanie konta... Czekaj 3 sek...";

        },
        onKeyUp: function (){
            var numer_konta = document.getElementById("numer_konta").value;
            var kwota = document.getElementById("amount").value;
        
            if ((numer_konta != NaN && isFinite(numer_konta))  && (kwota > 0) && kwota!= NaN && isFinite(kwota) ){
                document.getElementById("klik").disabled = false;
                }

            else{
                    document.getElementById("klik").disabled = true;   
                }
        }
    }
})();