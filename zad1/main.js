
var personFactory = function()
{
    var details = 
	{
        firstName: 'Marlena',
        lastName: 'Kwasnik',
        accountsList: 
        [{
            balance: 1000,
            currency: 'zloty'
          },

          {
            balance: 5000,
            currency: 'euro'
          }
        ]
    };
   // return console.log(details.firstName, details.lastName, sayHello());
  
   return  {
    firstName: details.firstName, 
    lastName: details.lastName,

    sayHello: function ()
        {
            return 'Name: ' +  details.firstName + ", Last Name: " + details.lastName + ", Account: " + details.accountsList.length ;
        }
   };

};

var person = personFactory();
console.log(person);
console.log(person.sayHello());





