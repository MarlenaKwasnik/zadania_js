var person = (function(){
    var details = 
    {
        firstName: 'Marlena',
        lastName: 'Kwasnik',
    };

    var calcu = function calculateBalance()
    {
       var sum = 0;
       var tmp = person.accountsList.length;
       for(i=0 ; i < tmp ;i++ )
       {
         sum = sum + person.accountsList[i].balance;
       }
       return sum;
    };


   return  {
    accountsList: 
    [{
        balance: 1000,
        currency: 'zloty'
      },

      {
        balance: 5000,
        currency: 'euro'
      }
    ],
    firstName: details.firstName, 
    imie: "Kasia",
    lastName: details.lastName,
    calcu,

    sayHello: function ()
        {
            return 'Name: ' +  this.imie + ", Last Name: " + this.lastName + ", Sum: " + calcu();
        },

    addAccount: function (account)
        {
           person.accountsList.push(account);
        }    
   }

})();

//console.log(person.addAccount());
console.log(person.sayHello());
person.addAccount(
    {
        balance: 300,
        currency: 'zloty'
    }
);
console.log(person.sayHello());