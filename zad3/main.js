
function Account(balance, currency) {
    this.balance = balance;
    this.currency = currency;
};

var person = (function(){
    
    var details = 
    {
        firstName: 'Marlena',
        lastName: 'Kwasnik',
    };

    var calcu = function calculateBalance()
    {
       var sum = 0;
       var tmp = person.accountsList.length;
       for(i=0 ; i < tmp ;i++ )
       {
         sum = sum + person.accountsList[i].balance;
       }
       return sum;
    };

   return  {
    accountsList: 
    [{
        balance: 1000,
        currency: 'zloty'
      },

      {
        balance: 5000,
        currency: 'euro'
      },
      konto = new Account(666, '$')
    ],
    firstName: details.firstName, 
    lastName: details.lastName,

    sayHello: function ()

    {
            return 'Name: ' +  details.firstName + ", Last Name: " + details.lastName + ", Sum: " + calcu();
        },

    addAccount: function (account)
        {
           return person.accountsList.push(account);
        }    
   }

})();

console.log(person.sayHello());
person.addAccount(
    {
        balance: 300,
        currency: 'zloty'
    }
);
console.log(person.sayHello());
console.log(person.accountsList[2]); 
